const contacts = [
{
    name: "Joseph",
    imgURL:
    "https://vignette.wikia.nocookie.net/starwars/images/e/eb/HanEndor.jpg/revision/latest/scale-to-width-down/340?cb=20080329135130",
    phone: "830-2370-877",
    email: "narniageek92@yahoo.com"
},
{
    name: "Jack Bauer",
    imgURL:
    "https://kids.kiddle.co/images/thumb/b/bc/Jack_Bauer1.jpg/300px-Jack_Bauer1.jpg",
    phone: "+123 456 789",
    email: "hungryhippo@yahoo.com"
},
{
    name: "Eva Green",
    imgURL:
    "https://vignette.wikia.nocookie.net/jamesbond/images/c/ca/Vesper_Lynd_%28Eva_Green%29_-_Profile.png/revision/latest/top-crop/width/720/height/900?cb=20161208175056",
    phone: "+555 55555",
    email: "imsorryjames@yahoo.com"
}
];

export default contacts;